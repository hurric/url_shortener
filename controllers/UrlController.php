<?php

namespace app\controllers;

use Yii;
use app\models\Url;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
/**
 * UrlController implements the CRUD actions for Url model.
 */
class UrlController extends Controller
{

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Url::find(),
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Url model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Url model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Url();

        if ($model->load(Yii::$app->request->post())) {
            //return slug if url is found in database. Check made after $model-load, to ensure data is validated
            $urlExist = Url::find()->where(['url' => $model->url])->one();

            if (!empty($urlExist))
              return $this->redirect(['url/index', 'slug' => $urlExist->slug]);

            if ($model->save())
              return $this->redirect(['url/index', 'slug' => $model->slug]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
    * takes slug from $_GET param, search for record in database and on succes redirects to corresponding url
    */
    public function actionRedirect()
    {
      $slug = Yii::$app->request->get('slug');

      if (!empty($slug))
      {
        $url = Url::find()->where(['slug' => $slug])->one();

        if (!empty($url->url))
          return $this->redirect($url->url);
      }
      return $this->render('slugNotFound');
    }

    /**
     * Finds the Url model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Url the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Url::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
