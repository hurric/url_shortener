<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\web\Response;
use app\models\Url;

class ApiController extends ActiveController
{
  public $modelClass = 'app\models\Url';

  public function actions()
  {
      // return [];
      $actions = parent::actions();
          unset(
            $actions['index'],
            $actions['create'],
            $actions['view'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
          );

          return $actions;
  }

  protected function verbs(){
        return [
            'index'=>['POST'],
            'create'=>['POST'],
        ];
    }

  public function beforeAction($action)
  {
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
  }

  public function afterAction($action, $result)
  {
      \Yii::$app->response->format = Response::FORMAT_JSON;
      return parent::afterAction($action, $result);
  }

  public function actionApi()
  {
    return true;
  }

  /**
  * Creates shortened links from url, or returns link to redirect from shortened link if one exist in database
  */
  public function actionCreate()
  {
    $long_url = \Yii::$app->request->post('long_url');
    $short_url = \Yii::$app->request->post('short_url');

    if (!empty($long_url))
    {
      $urlExist = Url::find()->where(['url' => $long_url])->one();
      if (!empty($urlExist->slug))
        return ["shortened link" => \yii\helpers\Url::base(true).'/'.$urlExist->slug];
      else {
        $model = new Url();
        $model->url = $long_url;
        if ($model->save())
          return ["shortened link" => \yii\helpers\Url::base(true).'/'.$model->slug];
        else return $model->errors;
      }
    }

    if (!empty($short_url))
    {
      $slug = basename($short_url);
      $slugExist = Url::find()->where(['slug' => $slug])->one();
      if (!empty($slugExist->url))
        return ["redirect link" => $slugExist->url];
      else return ["redirect link" => null];
    }

    return false;
  }
}
