<?php

namespace tests\unit\models;

use app\models\Url;
use app\tests\unit\fixtures\UrlFixture;

class UrlTest extends \Codeception\Test\Unit
{
    protected $tester;

    public function _fixtures() {
        return [
            'url' => 'app\tests\fixtures\UrlFixture',
        ];
    }

    function testTableName()
    {
         $this->assertEquals('url', Url::tableName());
    }

    public function testUrl()
    {
        $url = $this->tester->grabFixture('url','url1');
        $this->assertEquals('http://google.com', $url->url);
    }

    public function testCreateSlug()
    {
      $url = new Url();
      $slug = $url->CreateSlug(999999);
      $this->assertTrue($slug == '4c91');
    }

    public function testUrlResponseValidation()
    {
      $url = 'http://google.com';
      //strip url from HTTPS for localhost test
      if (YII_ENV == 'dev')
      {
        $charsToRemove = ["https://", "http://"];
        $url = str_replace($charsToRemove, "", $url);
      }

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_NOBODY, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch, CURLOPT_TIMEOUT,10);
      $output = curl_exec($ch);
      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);

      $this->assertFalse($httpcode == '404');
      $this->assertFalse($httpcode == '0');
    }
}
