<?php
class UrlCest
{
    public function _before(\FunctionalTester $I)
    {
        $I->amOnRoute('url/create');
    }

    public function openUrlPage(\FunctionalTester $I)
    {
        $I->see('Link shortener', 'h1');
    }

    public function submitEmptyForm(\FunctionalTester $I)
    {
        $I->submitForm('#w0', []);
        $I->expectTo('see validations errors');
        $I->see('Url cannot be blank.');
    }

    public function submitForm(\FunctionalTester $I)
    {
        $I->submitForm('#w0', ['Url[url]' => 'http://google.pl']);
        $I->see('Congratulations! Your shortened link is:', 'h3');
    }

    public function openNonExistingRedirectLink(\FunctionalTester $I) {
        $I->amOnRoute('url/redirect', ['slug' => 'aaa']);
        $I->see('Slug not found:(', 'h1');
    }

    public function openExistingRedirectLink(\FunctionalTester $I) {
      $I->amOnRoute('url/redirect', ['slug' => '1']);
      $I->dontSee('Slug not found:(', 'h1');
    }
}
