<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class UrlFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Url';
}
