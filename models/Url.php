<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "url".
 *
 * @property int $id
 * @property string $url
 * @property int $created_at
 * @property string $slug
 */
class Url extends \yii\db\ActiveRecord
{
  protected static $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'url';
    }

    public function behaviors()
    {
       return [
           'timestamp' => [
               'class' => 'yii\behaviors\TimestampBehavior',
               'attributes' => [
                   ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
               ],
           ],
       ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['url', 'required'],
            ['url', 'string', 'max' => 255],
            ['url', 'url'],
            ['url', 'urlResponseValidation',  'skipOnError' => false],
            ['slug', 'string', 'max' => 6],
            ['created_at', 'integer'],
        ];
    }

    public function urlResponseValidation($attribute,$params)
    {
      $url = urldecode($this->$attribute);
      //strip url from HTTPS for localhost test
      if (YII_ENV == 'dev')
      {
        $charsToRemove = ["https://", "http://"];
        $url = str_replace($charsToRemove, "", $url);
      }

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_NOBODY, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch, CURLOPT_TIMEOUT,10);
      $output = curl_exec($ch);
      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);

      if ($httpcode == '404' || $httpcode == 0)
        $this->addError($attribute, $attribute.' The URL address is dead.');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'created_at' => 'Created At',
            'slug' => 'Slug',
        ];
    }

    function afterSave($insert, $changedAttributes) {
      parent::afterSave($insert, $changedAttributes);
      if ($insert) {
          $this->slug = $this->CreateSlug($this->id);
          $this->save();
      }
    }

    function CreateSlug($id)
    {
      $length = strlen(self::$chars);
      $code = "";

      $chars = self::$chars;
      $length = strlen(self::$chars);
      $out = "";

      //convert $id to base-62
    	while($id > $length - 1)
    	{
    		$out = self::$chars[intval(fmod($id, $length))] . $out;
    		$id = floor( $id / $length );
    	}
      $slug = self::$chars[intval($id)] . $out;

      return $slug;
    }
}
