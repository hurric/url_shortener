# README #

### Brief ###
[readme](https://gist.github.com/ethanhinson/d388a239198f6eba8f5f863e4094c0c1)

### About ###

To fullfill task I chose Yii2 framework, which provides many helpfull classes and libraries from the get-go:

* data validation
* testing library (codeception)
* support for RESTful Web Service APIs
* security tools like csrf token, prevents sql injection sql injections, etc


In addition to help test website I have added view with links inserted in database (visible after crating shortened url). As for bonus points (for using a frontend framework like React, Vue, Angular, etc) - as I previously said, I don't have experience with those frameworks, but I included API, which can be used in any of this frameworks.

### Demo ###

[here](https://url-shortener.luk4.pl)

Adnotation: I know, that long subdomain seems to make links longer than shorter, it's just a demo site. You can try shorten very long URLs.

### Demo API ###

1. shorten link

```
curl -X POST -H 'Content-Type: application/json' -i 'http://url-shortener.luk4.pl/api' --data '{
"long_url":"http://google.com"
}'
```

2. retrieve url from shortened link

```
curl -X POST -H 'Content-Type: application/json' -i 'http://url-shortener.luk4.pl/api' --data '{
"short_url":"http://url-shortener.luk4.pl/URO"
}'
```

### How do I get set up? ###

1. clone repository
git clone https://hurric@bitbucket.org/hurric/url_shortener.git

2. install libraries via composer
composer install

3. create database for production with table:

```
CREATE TABLE `url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `created_at` int(11) unsigned NOT NULL,
  `slug` char(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_url_unique` (`url`) USING HASH,
  KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=218600 DEFAULT CHARSET=utf8mb4;
```

4. create database for testing with table:

```
CREATE TABLE `url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `created_at` int(11) unsigned NOT NULL,
  `slug` char(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_url_unique` (`url`) USING HASH,
  KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
```

5. configure DB credentials in url_shortener/config/db.php

6. configure DB credentials in url_shortener/config/test_db.php

7. run application:
php yii serve -p 3000

Application should be visible in browser on localhost address:
http://localhost:3000

### Unit tests and functional tests ###

All tests are in folder url_shortener/tests, to start test navigate to application and run command:

$ vendor/bin/codecept run

[image](https://luk4.pl/test.jpg)

### API ###

API can return shortened link from url or retrieve url to redirect from shortened link.

1. Create short link from url:

```
curl -X POST -H 'Content-Type: application/json' -i http://localhost:3000/api --data '{
"long_url":"http://test.com"
}'
```

returns {"shortened link" : SHORTENED LINK}

2. Retrieve url from shortened link

```
curl -X POST -H 'Content-Type: application/json' -i http://localhost:3000/api --data '{
"short_url":"http://localhost:3000/urp"
}'
```

if shortened link exist
returns {"redirect link" : REDIRECT LINK}

if doesn't:
returns {"redirect link" : null}

API also returns validation errors, if any occures.

### Limitations, bottlenecks ###

I've decided to make slug char of length 6, that way we can store more than 56 000 000 000 possible links (regarding that first ID is 218600, because of precautions with possible collisions with existing routes and created slugs - like http://localhost/api or http://localhost/url). Of course I could gave slug more length, I chose arbitrary, to know limits. On the other hand I could use datatype varchar, but there would be always extra 2 bytes for each record, and that could be a bit of unnecessary burden in large dataset.

### Database decision ###

Apart from char limit, I decided to set index on slug, I am assuming, that ratio of created links to used shortened links in real world scenario can be 1-5 or even 1-10, so database will be more frequently searched by slug.

### Why did I chose base63 for shortening links and other possible solutions ###

There was no requirement for hiding history of links, or rather making it difficult to spy on previous links. If that was the case, I would recommend one of the other solutions and add for example some time limit for users, who repeatedly use redirects.

other solutions are:

 - pregenerated random unique strings in second table, related to table with urls. This way we can also be sure, that we can't easily peek into to previous generated slug
 
 - random string generated when creating slug with uniqueness check, same as above, but potentialy can take more resources in later stages
