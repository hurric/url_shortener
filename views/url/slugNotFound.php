<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Urls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="url-index">

    <h1 class="text-center">Slug not found:(</h1>
    <h3 class="text-center">
      You can create one clicking <?= Html::a('here', ['url/index']); ?>!
    </h3>
</div>
