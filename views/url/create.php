<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Url */

$this->title = 'Create Url';
$this->params['breadcrumbs'][] = ['label' => 'Urls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="url-create">
    <h1 class="text-center">Link shortener</h1>
    <h3 class="text-center">Shorten Your link:</h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
