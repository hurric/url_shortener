<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Urls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="url-index">

    <h1 class="text-center">Link shortener</h1>

    <?php if (!empty(Yii::$app->request->get('slug'))): ?>
      <h3 class="text-center">Congratulations! Your shortened link is:</h3>
      <p class="text-center">
        <?= Html::a(Url::base(true).'/'.Yii::$app->request->get('slug'), ['/'.Yii::$app->request->get('slug')]); ?>
      </p>
      <br>
      <br>
    <?php endif; ?>

    <p>
        <?= Html::a('Shorten link', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'url:url',
            'created_at',
            'slug',
            [
              'label' => 'date',
              'value' => function ($model) {
                return date('Y-m-d h:i:s',$model->created_at);;
              }
            ],
            [
              'label' => 'Shortened',
              'format' => 'raw',
              'value' => function ($model) {
                return Html::a(Url::base(true).'/'.$model->slug, ['/'.$model->slug]);
              }
            ],

        ],
    ]); ?>


</div>
